import 'package:app_ioasys/app/app_controller.dart';
import 'package:app_ioasys/app/config/helpers/cache_helper.dart';
import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/app/data/source/remote/client/client.dart';
import 'package:app_ioasys/app/presentation/pages/home/home_module.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/sign_in_module.dart';
import 'package:app_ioasys/app/presentation/widgets/splash_screen.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:app_ioasys/app/app.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
    Bind((i) => AppController(i.get())),
    Bind((i) => TokenLocalDataSourceImpl(i.get())),
    Bind((i) => CacheHelperImpl()),
    Bind((i) => DioClient().getClient, singleton: false),
  ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => SplashScreen()),
        ModularRouter(SignInModule.route, module: SignInModule()),
        ModularRouter(HomeModule.route, module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
