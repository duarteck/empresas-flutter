import 'package:mobx/mobx.dart';

import 'data/source/local/token_local_data_source.dart';
part 'app_controller.g.dart';

class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase with Store {
  TokenLocalDataSource _tokenLocalDataSource;
  _AppControllerBase(this._tokenLocalDataSource);

  @observable
  OauthToken token;

  @action
  void setToken(OauthToken value) => token = value;

  @action
  Future<OauthToken> init() async {
    await Future.delayed(const Duration(seconds: 1));
    token = await _tokenLocalDataSource.getToken();
    return token;
  }
}