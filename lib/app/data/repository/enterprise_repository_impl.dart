import 'package:app_ioasys/app/data/source/remote/enterprise_remote_data_source.dart';
import 'package:app_ioasys/app/domain/entities/enterprise.dart';
import 'package:app_ioasys/core/repositories/enterprise_repository.dart';

class EnterpriseRepositoryImpl implements EnterpriseRepository{
  final EnterpriseRemoteDataSource _enterpriseRemoteDataSource;
  EnterpriseRepositoryImpl(this._enterpriseRemoteDataSource);

  @override
  Future<List<Enterprise>> getEnterpriseList(String filter) async{
    try{
      return await _enterpriseRemoteDataSource.getEnterpriseList(filter);
    } catch(e){
      return null;
    }
  }

}