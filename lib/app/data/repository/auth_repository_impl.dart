import 'package:app_ioasys/app/data/source/remote/auth_remote_data_source.dart';
import 'package:app_ioasys/app/domain/entities/credentials.dart';
import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/core/repositories/auth_repository.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource _authRemoteDataSource;
  AuthRepositoryImpl(this._authRemoteDataSource);

  @override
  Future<OauthToken> authenticate(Credentials credentials) async{
    try{
      return await _authRemoteDataSource.authenticate(credentials);
    } catch(e){
      return null;
    }
  }
}