import 'dart:convert';

import 'package:app_ioasys/app/config/helpers/cache_helper.dart';

class OauthToken {
  String accessToken;
  String client;
  String uid;

  OauthToken({this.accessToken, this.client, this.uid});

  factory OauthToken.fromJson(Map<String, dynamic> json) 
    => OauthToken(
      accessToken: json['accessToken'], 
      client: json['client'],
      uid: json['uid']);

  Map<String, dynamic> toJson() => {
    'accessToken' : accessToken,
    'client' : client,
    'uid' : uid
  };

  @override
  String toString() {
    return '[ ACCESS-TOKEN ] : $accessToken - [ CLIENT ] : $client - [ UID ] : $uid';
  }
}

abstract class TokenLocalDataSource {
  Future<OauthToken> getToken();
  Future<void> setToken(OauthToken token);
  Future<void> clear();
}

class TokenLocalDataSourceImpl implements TokenLocalDataSource {
  static const _TOKEN_KEY = 'token';
  final CacheHelper _cacheHelper;

  TokenLocalDataSourceImpl(this._cacheHelper);

  Future<OauthToken> getToken() async {
    final value = await _cacheHelper.getString(_TOKEN_KEY);

    if (value != null) return OauthToken.fromJson(jsonDecode(value));
    return null;
  }

  @override
  Future<void> setToken(OauthToken token) {
    return _cacheHelper.setString(_TOKEN_KEY, jsonEncode(token.toJson()));
  }

  @override
  Future<void> clear() => _cacheHelper.remove(_TOKEN_KEY);
}