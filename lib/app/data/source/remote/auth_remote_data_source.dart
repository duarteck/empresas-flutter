import 'dart:io';

import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/app/domain/entities/credentials.dart';
import 'package:dio/dio.dart';
import 'package:logging/logging.dart';

abstract class AuthRemoteDataSource {
  Future<OauthToken> authenticate(Credentials credentials);
}

class AuthRemoteDataSourceImpl implements AuthRemoteDataSource {
  final _log = Logger('AuthRemoteDataSource');
  
  final Dio _http;
  
  AuthRemoteDataSourceImpl(this._http);

  @override
  Future<OauthToken> authenticate(Credentials credentials) async{
    try{
      final response = await _http.post('/api/v1/users/auth/sign_in', 
        data: credentials.toJson());

      final token = response.headers.map['access-token'].first;
      final client = response.headers.map['client'].first;
      final uid = response.headers.map['uid'].first;

      final oauthToken = OauthToken(accessToken: token, client: client, uid: uid);

      _log.info('oauthToken ::: ${oauthToken.toString()}');

      return oauthToken;

    } on DioError catch(e){
      _log.warning('Error login ::: ${e.message}');
      throw HttpException(e.message);
    }
  }
}