import 'dart:io';

import 'package:app_ioasys/app/domain/entities/enterprise.dart';
import 'package:dio/dio.dart';

abstract class EnterpriseRemoteDataSource {
  Future<List<Enterprise>> getEnterpriseList(String filter);
}

class EnterpriseRemoteDataSourceImpl implements EnterpriseRemoteDataSource{
  final Dio _http;
  
  EnterpriseRemoteDataSourceImpl(this._http);

  @override
  Future<List<Enterprise>> getEnterpriseList(String filter) async{
    try{
      
      final response = await _http.get('/api/v1/enterprises',
        queryParameters: {
          'enterprise_types' : 2,
          'name' : filter
        });

      return (response.data['enterprises'] as List)
        ?.map((e) => Enterprise.fromJson(e))
        ?.toList();

    } on DioError catch(e){
      throw HttpException(e.message);
    }
  }

}