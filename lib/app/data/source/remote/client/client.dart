import 'dart:io';

import 'package:app_ioasys/app/app_controller.dart';
import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/sign_in_module.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_modular/flutter_modular.dart';

class DioClient {
  Dio _dio = Dio();

  DioClient() {
    _dio
      ..options.baseUrl = DotEnv().env["BASE_URL"]
      ..options.headers[HttpHeaders.contentTypeHeader] = Headers.jsonContentType;

    _dio.interceptors.add(_authenticationWrapper());
  }

  InterceptorsWrapper _authenticationWrapper() =>
      InterceptorsWrapper(onRequest: (options) async {
        final oauthToken = Modular.get<AppController>().token;

        if (oauthToken != null) {
          options.headers['access-token'] = oauthToken.accessToken;
          options.headers['client'] = oauthToken.client;
          options.headers['uid'] = oauthToken.uid;
        }

        return options;
      }, onError: (e) async {
        final oauthToken = Modular.get<AppController>().token;
        if(oauthToken == null) return e;

        if (e.response.statusCode == HttpStatus.unauthorized ||
            e.response.statusCode == HttpStatus.forbidden) {
          Modular.get<TokenLocalDataSource>().clear();
          Modular.to.pushNamedAndRemoveUntil(SignInModule.route, (r) => false);
        } else {
          return e;
        }
      });

  Dio get getClient => _dio;
}