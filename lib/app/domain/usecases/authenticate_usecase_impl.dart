import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/app/domain/entities/credentials.dart';
import 'package:app_ioasys/core/repositories/auth_repository.dart';
import 'package:app_ioasys/core/usecases/authenticate_usecase.dart';

class AuthenticateUseCaseImpl implements AuthenticateUseCase {
  final AuthRepository _authRepository;
  AuthenticateUseCaseImpl(this._authRepository);

  @override
  Future<OauthToken> call(Credentials credentials) async {
   return await _authRepository.authenticate(credentials);
  }

}