import 'package:app_ioasys/app/domain/entities/enterprise.dart';
import 'package:app_ioasys/core/repositories/enterprise_repository.dart';
import 'package:app_ioasys/core/usecases/list_enterprises_usecase.dart';

class ListEnterprisesUseCaseImpl implements ListEnterprisesUseCase {
  final EnterpriseRepository _enterpriseRepository;
  ListEnterprisesUseCaseImpl(this._enterpriseRepository);

  @override
  Future<List<Enterprise>> call(String filter) async{
    return await _enterpriseRepository.getEnterpriseList(filter);
  }
}