// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'enterprise.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Enterprise _$EnterpriseFromJson(Map<String, dynamic> json) {
  return Enterprise(
    id: json['id'] as int,
    enterpriseName: json['enterprise_name'] as String,
    photo: json['photo'] as String,
    description: json['description'] as String,
  );
}

Map<String, dynamic> _$EnterpriseToJson(Enterprise instance) =>
    <String, dynamic>{
      'id': instance.id,
      'enterprise_name': instance.enterpriseName,
      'photo': instance.photo,
      'description': instance.description,
    };
