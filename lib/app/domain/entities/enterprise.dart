import 'package:json_annotation/json_annotation.dart';

part 'enterprise.g.dart';

@JsonSerializable()
class Enterprise {
  final int id;
  
  @JsonKey(name: 'enterprise_name')
  final String enterpriseName;

  final String photo;
  final String description;

  Enterprise({this.id, this.enterpriseName, this.photo, this.description});

  factory Enterprise.fromJson(Map<String, dynamic> json) => _$EnterpriseFromJson(json);

  Map<String, dynamic> toJson() => _$EnterpriseToJson(this);
}