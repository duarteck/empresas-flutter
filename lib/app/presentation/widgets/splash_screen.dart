import 'package:app_ioasys/app/app_controller.dart';
import 'package:app_ioasys/app/config/constants/assets.dart';
import 'package:app_ioasys/app/presentation/pages/home/home_module.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/sign_in_module.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Modular.get<AppController>().init().then((tokens) {
      if (tokens != null) {
        Modular.to.pushReplacementNamed(HomeModule.route);
      } else {
        Modular.to.pushReplacementNamed(SignInModule.route);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.maxFinite,
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(AppAssets.background)
        )
      ),
      child: Image.asset(AppAssets.logoSplashScreen, scale: 1),
    );
  }
}