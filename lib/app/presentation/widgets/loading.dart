import 'package:app_ioasys/app/config/constants/assets.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class Loading extends StatefulWidget {
  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading>  with TickerProviderStateMixin{
  AnimationController _controller1;
  AnimationController _controller2;

  @override
  void initState() {
    super.initState();

    _controller1 = AnimationController(vsync: this, duration: Duration(seconds: 2))..repeat();
    _controller2 = AnimationController(vsync: this, duration: Duration(seconds: 7))..repeat();
  }

  @override
  void dispose() {
    _controller1.dispose();
    _controller2.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: Alignment.center,
        children: [
          AnimatedBuilder(
              animation: _controller1,
              builder: (_, child) {
                return Transform.rotate(
                  angle: _controller1.value * 2 * math.pi,
                  child: child,
                );
              },
              child: Image.asset(AppAssets.ellipse, scale: 1),
            ),

             AnimatedBuilder(
              animation: _controller2,
              builder: (_, child) {
                return Transform.rotate(
                  angle: _controller2.value * 6.3,
                  child: child,
                );
              },
              child: Image.asset(AppAssets.ellipse, scale: .6,),
            ),
        ],
      ),
    );
  }
}