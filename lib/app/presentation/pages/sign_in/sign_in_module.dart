import 'package:app_ioasys/app/data/repository/auth_repository_impl.dart';
import 'package:app_ioasys/app/data/source/remote/auth_remote_data_source.dart';
import 'package:app_ioasys/app/domain/usecases/authenticate_usecase_impl.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/sign_in_controller.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/sign_in_page.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SignInModule extends ChildModule {
  static const String route = '/SignInModule';

  @override
  List<Bind> get binds => [
    Bind((i) => SignInController(i.get(), i.get(), i.get())),
    Bind((i) => AuthenticateUseCaseImpl(i.get())),
    Bind((i) => AuthRepositoryImpl(i.get())),
    Bind((i) => AuthRemoteDataSourceImpl(i.get())),
  ];

  @override
  List<ModularRouter> get routers => [
    ModularRouter(Modular.initialRoute, child: (_, args) => SignInPage())
  ];

  static Inject get to => Inject<SignInModule>.of();

}