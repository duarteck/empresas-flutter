import 'package:app_ioasys/app/config/constants/colors.dart';
import 'package:app_ioasys/app/config/constants/strings.dart';
import 'package:app_ioasys/app/config/helpers/utils.dart';
import 'package:app_ioasys/app/presentation/pages/home/home_module.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/sign_in_controller.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/widgets/custom_button.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/widgets/custom_text_field.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/widgets/header_sign_in.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends ModularState<SignInPage, SignInController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            HeaderSignIn(),
              
            Observer(builder: (_) => CustomTextField(
              onTapIconError: controller.removeErrorLogin,
              hasError: controller.errorLogin,
              label: AppStrings.email,
              onChange: controller.setEmail
            )),

            Observer(builder: (_) => CustomTextField(
              onTapIconError: controller.removeErrorLogin,
              hasError: controller.errorLogin,
              label: AppStrings.password,
              suffixIcon: IconButton(
                icon: Icon(controller.visiblePassword
                  ? Icons.visibility_off 
                  : Icons.visibility, 
                color: AppColors.hintColor), 
                onPressed: controller.updateVisiblePassword),
              obscureText: !controller.visiblePassword,
              onChange: controller.setPassword,
              showLabelError: controller.errorLogin,
            )),
            
            CustomButton(
              label: AppStrings.enter,
              onPressed: login,
            )

          ],
        ),
      ),
    );
  }

  Future<void> login() async{
    FocusScope.of(context).unfocus();
    Utils.of(context).showLoading();

    final success = await controller.login();

    Modular.to.pop();

    if(success){
      Modular.to.pushNamedAndRemoveUntil(HomeModule.route, (r) => false);
    }
  }
}