import 'package:app_ioasys/app/app_controller.dart';
import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/app/domain/entities/credentials.dart';
import 'package:app_ioasys/core/usecases/authenticate_usecase.dart';
import 'package:logging/logging.dart';
import 'package:mobx/mobx.dart';
part 'sign_in_controller.g.dart';

class SignInController = _SignInControllerBase with _$SignInController;

abstract class _SignInControllerBase with Store {
  final _log = Logger('SignInController');

  final AuthenticateUseCase _authenticateUseCase;
  final AppController _appController;
  final TokenLocalDataSource _tokenLocalDataSource;

  _SignInControllerBase(
    this._authenticateUseCase, 
    this._appController, 
    this._tokenLocalDataSource);
  
  @observable
  bool visiblePassword = false;

  @observable
  bool errorLogin = false;
  
  @observable
  String email;

  @observable
  String password;

  @computed
  Credentials get _credentials => Credentials(email: email, password: password);

  @action
  void setEmail(String value) {
    errorLogin = false;
    email = value;
  }

  @action
  void setPassword(String value) {
    errorLogin = false;
    password = value;
  } 

  @action
  void removeErrorLogin() => errorLogin = false;

  @action
  void updateVisiblePassword() => visiblePassword = !visiblePassword;

  @action
  Future<bool> login() async {
    _log.info('Efetuando login ::: ${_credentials.toString()}');

    if (!validadeFields()) {
      errorLogin = true;
      return false;
    } 

    final response = await _authenticateUseCase(_credentials);

    if (response != null) {
      _appController.token = response;
      _tokenLocalDataSource.setToken(response);
    } else {
      errorLogin = true;
    }

    return response != null;
  }

  bool validadeFields() {
    if (email == null || email.isEmpty || !email.contains('@')) {
      return false;
    }

    if (password == null || password.isEmpty) {
      return false;
    }

    return true;
  }
}