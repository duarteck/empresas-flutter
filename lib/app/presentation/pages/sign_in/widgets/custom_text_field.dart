import 'package:app_ioasys/app/config/constants/assets.dart';
import 'package:app_ioasys/app/config/constants/colors.dart';
import 'package:app_ioasys/app/config/constants/fonts.dart';
import 'package:app_ioasys/app/config/constants/strings.dart';
import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String label;
  final Function(String) onChange;
  final Function onTapIconError;
  final bool obscureText;
  final bool hasError;
  final Widget suffixIcon;
  final bool showLabelError;

  const CustomTextField({
    @required this.label, 
    Key key, 
    this.onChange,
    this.obscureText = false, 
    this.suffixIcon, 
    this.hasError = false, 
    this.showLabelError = false, this.onTapIconError}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 0, 16, 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 4),
            child: Text(label,
              style: TextStyle(
                fontSize: 14,
                color: AppColors.hintColor,
                fontFamily: AppFonts.regular)),
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: hasError 
                ? Colors.red 
                : Colors.transparent),
              borderRadius: BorderRadius.circular(4)
            ),
            child: TextFormField(
              onChanged: onChange,
              obscureText: obscureText,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(12),
                fillColor: AppColors.gray,
                filled: true,
                suffixIcon: hasError 
                  ? GestureDetector(
                      onTap: () => onTapIconError(),
                      child: Image.asset(AppAssets.error)) 
                  : suffixIcon,
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(4)
                ),
              ),
            ),
          ),
          Visibility(
            visible: showLabelError,
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              margin: const EdgeInsets.only(top: 4),
              width: double.maxFinite,
              child: Text(AppStrings.invalidCredentials,
                textAlign: TextAlign.end,
                style: TextStyle(
                  color: AppColors.red,
                  fontSize: 12, 
                  fontFamily: AppFonts.light)),
            ))
        ],
      ),
    );
  }
}