import 'package:app_ioasys/app/config/constants/fonts.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  final Function onPressed;
  final String label;

  const CustomButton({@required this.label, Key key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 31, vertical: 24),
      child: RaisedButton(
        focusElevation: 0,
        highlightElevation: 0,
        elevation: 0,
        onPressed: () => onPressed(), 
        child: Text('$label', 
          style: TextStyle(
            fontSize: 16, 
            fontFamily: AppFonts.medium))),
    );
  }
}