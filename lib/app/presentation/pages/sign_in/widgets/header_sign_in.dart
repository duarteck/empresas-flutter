import 'package:app_ioasys/app/config/constants/assets.dart';
import 'package:app_ioasys/app/config/constants/fonts.dart';
import 'package:app_ioasys/app/config/constants/strings.dart';
import 'package:flutter/material.dart';

class HeaderSignIn extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final paddingTop = MediaQuery.of(context).padding.top;
    return ClipPath(
      clipper: BottomOvalClipper(),
      child: Container(
        padding: EdgeInsets.only(bottom: 130, top: paddingTop + 69),
        width: double.maxFinite,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(AppAssets.background)
          )
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 16.42),
              child: Image.asset(AppAssets.logoHome),
            ),
            Text(AppStrings.welcome,
              style: TextStyle(
                fontSize: 20, 
                fontFamily: AppFonts.regular, 
                color: Colors.white))
          ],
        ),
      ),
    );
  }
}

class BottomOvalClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {

    final path = Path();

    path.lineTo(0.0, size.height - 80);

    final firstControlPoint = Offset(size.width / 2, size.height);
    final firstEndPoint = Offset(size.width, size.height - 80);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    path.lineTo(size.width, 0.0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}