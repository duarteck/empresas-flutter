import 'package:app_ioasys/app/config/constants/assets.dart';
import 'package:flutter/material.dart';

class BackgroundAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.maxFinite,
      width: double.maxFinite,
      margin: const EdgeInsets.only(bottom: 24),
      decoration: BoxDecoration(
        image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage(AppAssets.background)
        )
      ),
      child: Stack(
        children: [
          Positioned(
            left: 0,
            top: 80,
            child: Image.asset(AppAssets.logo1, height: 150, fit: BoxFit.contain)),
          Positioned(
            left: 40,
            child: Image.asset(AppAssets.logo2, height: 180, fit: BoxFit.contain)),
          Positioned(
            top: 90,
            right: 50,
            child: Image.asset(AppAssets.logo3, height: 130, fit: BoxFit.contain)),
          Positioned(
            right: 0,
            top: 0,
            child: Image.asset(AppAssets.logo4, height: 140, fit: BoxFit.contain)),
        ],
      ),
    );
  }
}