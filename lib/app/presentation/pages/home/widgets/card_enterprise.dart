import 'package:app_ioasys/app/config/constants/colors.dart';
import 'package:app_ioasys/app/config/constants/fonts.dart';
import 'package:app_ioasys/app/domain/entities/enterprise.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class CardEnterprise extends StatelessWidget {
  final Enterprise enterprise;
  final Function onTap;

  const CardEnterprise({Key key, this.enterprise, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){},
      child: Container(
        margin: const EdgeInsets.only(bottom: 8),
        padding: const EdgeInsets.only(bottom: 8),
        width: double.maxFinite,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: AppColors.pink
        ),
        child: Column(
          children: [
            InkWell(
              onTap: () => onTap(),
              child: CachedNetworkImage(
                imageBuilder: (_, image) => Container(
                  margin: const EdgeInsets.only(bottom: 8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(top: Radius.circular(4)),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: image
                    )
                  ),
                ),
                height: 150,
                imageUrl: '${DotEnv().env["BASE_URL"]}${enterprise.photo}'),
            ),

            Text('${enterprise.enterpriseName}',
              style: TextStyle(fontSize: 18, fontFamily: AppFonts.bold, color: Colors.white))
            
          ],
        ),
      ),
    );
  }
}