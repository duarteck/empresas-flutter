import 'package:app_ioasys/app/config/constants/assets.dart';
import 'package:app_ioasys/app/config/constants/colors.dart';
import 'package:app_ioasys/app/config/constants/fonts.dart';
import 'package:app_ioasys/app/domain/entities/enterprise.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_modular/flutter_modular.dart';

class DetailEnterprise extends StatelessWidget {
  static const String route = '/HomeModule/DetailEnterprise';

  final Enterprise enterprise;

  const DetailEnterprise({Key key, this.enterprise}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${enterprise.enterpriseName?? ""}'),
        leading: Padding(
          padding: const EdgeInsets.only(left: 8, top: 4, bottom: 8),
          child: FlatButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
            ),
            color: AppColors.gray,
            splashColor: AppColors.pink.withOpacity(.1),
            padding: const EdgeInsets.all(2),
            onPressed: () => Modular.to.pop(),
            child: Image.asset(AppAssets.arrowBack)),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            CachedNetworkImage(
                imageBuilder: (_, image) => Container(
                  margin: const EdgeInsets.only(bottom: 8),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: image
                    )
                  ),
                ),
                height: 150,
                imageUrl: '${DotEnv().env["BASE_URL"]}${enterprise.photo}'),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 18),
              child: Text('${enterprise.description?? ""}',
                style: TextStyle(
                  fontSize: 18, 
                  fontFamily: AppFonts.light,
                  height: 1.3, 
                  color: Colors.black)),
            )
          ],
        ),
      ),
    );
  }
}