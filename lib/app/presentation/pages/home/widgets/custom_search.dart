import 'package:app_ioasys/app/config/constants/colors.dart';
import 'package:app_ioasys/app/config/constants/strings.dart';
import 'package:flutter/material.dart';

class CustomSearch extends StatelessWidget {
  final Function onTap;
  final Function(String) onChanged;

  const CustomSearch({Key key, this.onTap, this.onChanged}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: TextFormField(
        onChanged: onChanged,
        decoration: InputDecoration(
          hintText: AppStrings.searchEnterprise,
          prefixIcon: Icon(Icons.search, color: AppColors.hintColor, size: 30),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(4)
          ),
          filled: true,
          fillColor: AppColors.gray,
          contentPadding: const EdgeInsets.all(12)
        ),
      ),
    );
  }
}