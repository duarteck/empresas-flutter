import 'package:app_ioasys/app/data/repository/enterprise_repository_impl.dart';
import 'package:app_ioasys/app/data/source/remote/enterprise_remote_data_source.dart';
import 'package:app_ioasys/app/domain/usecases/list_enterprises_usecase_impl.dart';
import 'package:app_ioasys/app/presentation/pages/home/widgets/detail_enterprise.dart';

import 'home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'home_page.dart';

class HomeModule extends ChildModule {
  static const String route = '/HomeModule';

  @override
  List<Bind> get binds => [
    Bind((i) => HomeController(i.get())),
    Bind((i) => ListEnterprisesUseCaseImpl(i.get())),
    Bind((i) => EnterpriseRepositoryImpl(i.get())),
    Bind((i) => EnterpriseRemoteDataSourceImpl(i.get())),
  ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => HomePage()),
        ModularRouter('/DetailEnterprise', child: (_, args) => DetailEnterprise(enterprise: args.data)),
      ];

  static Inject get to => Inject<HomeModule>.of();
}