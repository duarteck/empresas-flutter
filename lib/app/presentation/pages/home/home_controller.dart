import 'package:app_ioasys/app/domain/entities/enterprise.dart';
import 'package:app_ioasys/core/usecases/list_enterprises_usecase.dart';
import 'package:mobx/mobx.dart';
part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final ListEnterprisesUseCase _listEnterprisesUseCase;
  _HomeControllerBase(this._listEnterprisesUseCase);

  @observable
  ObservableFuture<List<Enterprise>> futureListEnterprise = ObservableFuture.value([]);

  @observable
  ObservableList<Enterprise> listEnterprise = <Enterprise>[].asObservable();

  @action
  Future<void> searchEnterprise(String value) async{
    listEnterprise.clear();

    if(value.isEmpty) return;

    futureListEnterprise = _listEnterprisesUseCase(value).asObservable();

    await futureListEnterprise;

    listEnterprise.addAll(futureListEnterprise.value?? []);

  }
}