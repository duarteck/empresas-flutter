// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$futureListEnterpriseAtom =
      Atom(name: '_HomeControllerBase.futureListEnterprise');

  @override
  ObservableFuture<List<Enterprise>> get futureListEnterprise {
    _$futureListEnterpriseAtom.reportRead();
    return super.futureListEnterprise;
  }

  @override
  set futureListEnterprise(ObservableFuture<List<Enterprise>> value) {
    _$futureListEnterpriseAtom.reportWrite(value, super.futureListEnterprise,
        () {
      super.futureListEnterprise = value;
    });
  }

  final _$listEnterpriseAtom = Atom(name: '_HomeControllerBase.listEnterprise');

  @override
  ObservableList<Enterprise> get listEnterprise {
    _$listEnterpriseAtom.reportRead();
    return super.listEnterprise;
  }

  @override
  set listEnterprise(ObservableList<Enterprise> value) {
    _$listEnterpriseAtom.reportWrite(value, super.listEnterprise, () {
      super.listEnterprise = value;
    });
  }

  final _$searchEnterpriseAsyncAction =
      AsyncAction('_HomeControllerBase.searchEnterprise');

  @override
  Future<void> searchEnterprise(String value) {
    return _$searchEnterpriseAsyncAction
        .run(() => super.searchEnterprise(value));
  }

  @override
  String toString() {
    return '''
futureListEnterprise: ${futureListEnterprise},
listEnterprise: ${listEnterprise}
    ''';
  }
}
