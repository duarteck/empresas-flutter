import 'package:app_ioasys/app/config/constants/colors.dart';
import 'package:app_ioasys/app/config/constants/fonts.dart';
import 'package:app_ioasys/app/config/constants/strings.dart';
import 'package:app_ioasys/app/presentation/pages/home/home_controller.dart';
import 'package:app_ioasys/app/presentation/pages/home/widgets/background_app_bar.dart';
import 'package:app_ioasys/app/presentation/pages/home/widgets/card_enterprise.dart';
import 'package:app_ioasys/app/presentation/pages/home/widgets/custom_search.dart';
import 'package:app_ioasys/app/presentation/pages/home/widgets/detail_enterprise.dart';
import 'package:app_ioasys/app/presentation/widgets/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  final _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      body: CustomScrollView(
        controller: _scrollController,
        slivers: [
          
          SliverAppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            pinned: true,
            expandedHeight: size.height * .3,
            flexibleSpace: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  BackgroundAppBar(),
                  CustomSearch(
                    onChanged: (value){
                      controller.searchEnterprise(value);
                      _scrollController.animateTo(
                        size.height * .22,
                        duration: Duration(milliseconds: 200), 
                        curve: Curves.easeOut);
                    },
                  ),
                ],
              ),
          ),

          SliverFillRemaining(
            hasScrollBody: false,
            child: Observer(builder: (_) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                children: [
                  
                  Visibility(
                    visible: controller.listEnterprise.length > 0,
                    child: Container(
                      margin: const EdgeInsets.symmetric(vertical: 16),
                      alignment: Alignment.centerLeft,
                      child: Text('0${controller.listEnterprise.length} resultados encontrados',
                        style: TextStyle(
                          fontSize: 14, 
                          fontFamily: AppFonts.light, 
                          color: AppColors.hintColor)
                      )
                    )
                  ),

                  Visibility(
                    visible: controller.listEnterprise.length == 0 && 
                      controller.futureListEnterprise.status != FutureStatus.pending,
                    child: Container(
                      height: MediaQuery.of(context).size.height * .6,
                      alignment: Alignment.center,
                      child: Text(AppStrings.emptyResult))
                  ),

                  ...List.generate(controller.listEnterprise.length, 
                    (i) => CardEnterprise(
                      enterprise: controller.listEnterprise[i],
                      onTap: () {
                        FocusScope.of(context).unfocus();
                        Modular.to.pushNamed(DetailEnterprise.route, arguments: controller.listEnterprise[i]);
                      },
                    )
                  ),

                  Visibility(
                    visible: controller.futureListEnterprise.status == FutureStatus.pending,
                    child: IntrinsicHeight(
                      child: Container(
                        alignment: Alignment.center,
                        child: Loading()),
                    )
                  ),
                ],
              ),
            )),
          )
        ],
      ),
    );
  }
}