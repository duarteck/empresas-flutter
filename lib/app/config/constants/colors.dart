import 'dart:ui';

class AppColors {
  static const Color pink = Color(0xFFE01E69);
  static const Color gray = Color(0xFFF5F5F5);
  static const Color red = Color(0xFFE00000);
  static Color hintColor = Color(0xFF666666).withOpacity(.7);
}