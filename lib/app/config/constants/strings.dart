class AppStrings {
  static const String searchEnterprise = 'Pesquise por empresa';
  static const String email = 'Email';
  static const String password = 'Senha';
  static const String enter = 'ENTRAR';
  static const String invalidCredentials = 'Credenciais incorretas';
  static const String emptyResult = 'Nenhum resultado encontrado';
  static const String welcome = 'Seja bem vindo ao empresas!';
}