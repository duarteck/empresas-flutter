class AppAssets {
  static const String background = 'assets/images/background.png';
  static const String logoSplashScreen = 'assets/images/logo_splashscreen.png';
  static const String logoHome = 'assets/images/logo_home.png';
  static const String logo1 = 'assets/images/logo_1.png';
  static const String logo2 = 'assets/images/logo_2.png';
  static const String logo3 = 'assets/images/logo_3.png';
  static const String logo4 = 'assets/images/logo_4.png';
  static const String ellipse = 'assets/images/ellipse.png';
  static const String error = 'assets/images/error.png';
  static const String arrowBack = 'assets/images/arrow.png';
}