import 'package:app_ioasys/app/presentation/widgets/loading.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Utils {
  final BuildContext _context;
  
  Utils.of(this._context);

  void showLoading() => showDialog(
    barrierDismissible: false,
    useSafeArea: true,
    context: _context,
    builder: (_) => WillPopScope(
      onWillPop: () async => false,
      child: Loading()));
}