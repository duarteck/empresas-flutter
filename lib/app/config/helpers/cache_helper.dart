import 'package:shared_preferences/shared_preferences.dart';

abstract class CacheHelper {
  Future<String> getString(String key);
  Future setString(String key, String value);
  Future<void> remove(String key);
}

class CacheHelperImpl implements CacheHelper {
  Future<SharedPreferences> _getInstance() => SharedPreferences.getInstance();

  Future<String> getString(String key) async {
    final cache = await _getInstance();
    return cache.getString(key);
  }

  Future<void> setString(String key, String value) async {
    final cache = await _getInstance();
    cache.setString(key, value);
  }

  Future<void> remove(String key) async {
    final cache = await _getInstance();
    cache.remove(key);
  }
}