import 'package:app_ioasys/app/config/constants/colors.dart';
import 'package:app_ioasys/app/config/constants/fonts.dart';
import 'package:flutter/material.dart';

class AppTheme {

  ThemeData get themeData => ThemeData(
      backgroundColor: Colors.white,
      textTheme: _textTheme,
      scaffoldBackgroundColor: Colors.white,
      appBarTheme: _appBarTheme,
      cursorColor: AppColors.pink,
      buttonTheme: _buttonTheme);

  TextTheme get _textTheme => const TextTheme(
        headline1: TextStyle(fontSize: 56),
        headline2: TextStyle(fontSize: 48),
        headline3: TextStyle(fontSize: 40),
        headline4: TextStyle(fontSize: 32),
        headline5: TextStyle(fontSize: 28),
        headline6: TextStyle(fontSize: 20, fontFamily: AppFonts.medium),
        bodyText1: TextStyle(fontSize: 16, fontFamily: AppFonts.light),
        bodyText2: TextStyle(fontSize: 18, fontFamily: AppFonts.light),
        caption: TextStyle(fontSize: 14, fontFamily: AppFonts.regular),
      );

  AppBarTheme get _appBarTheme => AppBarTheme(
        elevation: 0,
        centerTitle: true,
        color: Colors.white,
        textTheme: TextTheme(
          headline6: TextStyle(
            fontSize: 20,
            fontFamily: AppFonts.medium,
            color: Colors.black
          ),
        ),
      );

  ButtonThemeData get _buttonTheme => ButtonThemeData(
        minWidth: double.infinity,
        height: 48,
        buttonColor: AppColors.pink,
        textTheme: ButtonTextTheme.primary,
        shape: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(8))),
        padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 21),
      );
}
