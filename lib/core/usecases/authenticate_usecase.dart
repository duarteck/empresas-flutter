import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/app/domain/entities/credentials.dart';

abstract class AuthenticateUseCase {
  Future<OauthToken> call(Credentials credentials);
}