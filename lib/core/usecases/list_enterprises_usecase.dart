import 'package:app_ioasys/app/domain/entities/enterprise.dart';

abstract class ListEnterprisesUseCase {
  Future<List<Enterprise>> call(String filter);
}