import 'package:app_ioasys/app/domain/entities/enterprise.dart';

abstract class EnterpriseRepository {
  Future<List<Enterprise>> getEnterpriseList(String filter);
}