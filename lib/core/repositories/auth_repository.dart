import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/app/domain/entities/credentials.dart';

abstract class AuthRepository {
  Future<OauthToken> authenticate(Credentials credentials);
}