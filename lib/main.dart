import 'package:flutter/material.dart';
import 'package:app_ioasys/app/app_module.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:logging/logging.dart';

Future main() async {
  // Modo retrato obrigatorio
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  Logger.root.level = Level.INFO; // defaults to Level.INFO
  Logger.root.onRecord.listen((record) {
    debugPrint(
        '[ ${record.level.name} ] : ${record.loggerName} : ${record.message}');
  });

  // Configuracao da variavel de ambiente
  await DotEnv().load(".env");

  runApp(ModularApp(module: AppModule()));
}
