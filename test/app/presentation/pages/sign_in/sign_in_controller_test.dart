import 'package:app_ioasys/app/app_controller.dart';
import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:app_ioasys/app/domain/entities/credentials.dart';
import 'package:app_ioasys/app/presentation/pages/sign_in/sign_in_controller.dart';
import 'package:app_ioasys/core/usecases/authenticate_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockTokenDataSource extends Mock implements TokenLocalDataSource {}
class MockAuthenticateUseCase extends Mock implements AuthenticateUseCase {}

const String email = 'email@email.com';
const String password = 'PASSWORD';

void main() {
  AuthenticateUseCase authenticateUseCase;
  AppController appController;
  MockTokenDataSource tokenDataSource;
  SignInController signInController;

  setUp((){
    tokenDataSource = MockTokenDataSource();
    appController = AppController(tokenDataSource);
    authenticateUseCase = MockAuthenticateUseCase();
    signInController = SignInController(authenticateUseCase, appController, tokenDataSource);
  });

  test('Quando adicionar um email, o parametro email não deve ser nulo', () {
    
    signInController.setEmail(email);

    expect(signInController.email, email);
  });

  test('Quando adicionar uma senha, o parametro password não deve ser nulo', () {
    
    signInController.setPassword(password);

    expect(signInController.password, password);
  });

  test('Quando eu chamar o metodo removerErrorLogin, o parametro errorLogin deve ser false', () {
    
    signInController.removeErrorLogin();

    expect(signInController.errorLogin, false);
  });
  
  test('Quando eu chamar o metodo updateVisiblePassword, o parametro visiblePassword deve ser true', () {
    
    signInController.updateVisiblePassword();

    expect(signInController.visiblePassword, true);
  });

}