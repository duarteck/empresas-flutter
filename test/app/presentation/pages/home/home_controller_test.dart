import 'package:app_ioasys/app/domain/entities/enterprise.dart';
import 'package:app_ioasys/app/presentation/pages/home/home_controller.dart';
import 'package:app_ioasys/core/usecases/list_enterprises_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockListEnterprisesUseCase extends Mock implements ListEnterprisesUseCase {}

void main() {
  HomeController homeController;
  ListEnterprisesUseCase listEnterprisesUseCase;

  setUp((){
    listEnterprisesUseCase = MockListEnterprisesUseCase();
    homeController = HomeController(listEnterprisesUseCase);
  });

  test('Quando eu digitar um texto, a função deve me retornar uma lista de empresas', () async{
    final listEnterprise = List.generate(10, (i) => Enterprise());

    when(listEnterprisesUseCase('a')).thenAnswer((_) => Future.value(listEnterprise));

    await homeController.searchEnterprise('a');

    expect(homeController.listEnterprise.length, 10);
  });
  
}