import 'package:app_ioasys/app/app_controller.dart';
import 'package:app_ioasys/app/data/source/local/token_local_data_source.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockTokenDataSource extends Mock implements TokenLocalDataSource {}

void main() {
  OauthToken oauthToken;
  AppController appController;
  MockTokenDataSource tokenDataSource;

  setUp((){
    oauthToken = OauthToken(accessToken: 'TOKEN', client: 'CLIENT', uid: 'UID');
    tokenDataSource = MockTokenDataSource();
    appController = AppController(tokenDataSource);
  });

  test('Quando adicionar um oauthToken, o parametro token não deve ser nulo', () {
    
    appController.setToken(oauthToken);

    expect(appController.token.accessToken, oauthToken.accessToken);
    expect(appController.token.client, oauthToken.client);
    expect(appController.token.uid, oauthToken.uid);
  });

  test('Quando chamar a função init(), a função deve retornar um objeto oauthToken', () async{
    
    when(tokenDataSource.getToken())
      .thenAnswer((_) => Future.value(oauthToken));

    final token = await appController.init();

    expect(token.accessToken, oauthToken.accessToken);
    expect(token.client, oauthToken.client);
    expect(token.uid, oauthToken.uid);
    verify(tokenDataSource.getToken()).called(1);
  });

  test('Quando chamar a função init(), a função deve retornar um objeto oauthToken nulo', () async{
    
    when(tokenDataSource.getToken())
      .thenAnswer((_) => Future.value(null));

    final token = await appController.init();

    expect(token, null);
    verify(tokenDataSource.getToken()).called(1);
  });

}
